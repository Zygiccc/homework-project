<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::get('/', 'MovieController@index');

/* 
*	Route::get('/', function () {
*     return view('home');
* });
*/

Route::resource('/movies', 'MovieController');

Route::get('/formos', 'FormController@index');
Route::post('/formos', 'FormController@displayRequest');

Route::resource('/user', 'UserController');

Route::get('/home', 'HomeController@index');

Auth::routes();
