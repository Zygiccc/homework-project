@extends("layout.main")

@section("content")

	<ul>

	<li>{{ $movie['title'] }}</li>
	<li>{{ $movie['description'] }}</li>
	<li><img src="{{ $movie->image }}"></li>
	<li>{{ $movie['year'] }}</li>
	<li>{{ $movie['duration'] }}</li>
	@if(\Auth::guest() || (!\Auth::guest() && \Auth::user()->is_admin == 0))
	@else
	<a href="{{ route('movies.edit', $movie->id) }}">Edit</a>
	@endif
	<hr>

</ul>

@endsection