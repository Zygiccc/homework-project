@extends("layout.main")

@section("content")


<ul>
@foreach($movies as $movie)

	<li>{{ $movie['title'] }}</li>
	<li>{{ $movie['description'] }}</li>
	<li> <a href="{{ route('movies.show', $movie->id) }}"> <img src="{{ $movie['image'] }}"></a></li>
	<li>Year {{ $movie['year'] }}</li>
	<li>{{ $movie['duration'] }} min.</li>
	@if(\Auth::guest() || (!\Auth::guest() && \Auth::user()->is_admin == 0))
	@else
	<a href="{{ route('movies.edit', $movie->id) }}">Edit</a>
	@endif
	<hr>

@endforeach
</ul>

@endsection