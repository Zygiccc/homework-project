@extends("layout.main")

@section("content")

<h3>Add Movie</h3>



	@if(isset($movie))
	{{Form::open(['route' => ['movies.update', $movie->id], 'method' => 'POST'])}}
	{{Form::hidden('_method', 'PUT')}}
	@else
	{{Form::open(['route' => 'movies.store', 'method' => 'POST'])}}
	@endif

	{{ csrf_field() }}

	{{Form::label('title', 'Title:')}}
	@if(isset($movie))
	{{Form::text('title', ($movie['title']), ['class'=>'form-control'])}}
	@else
	{{Form::text('title', '', ['class'=>'form-control'])}}
	@endif

	{{Form::label('duration', 'Duration:')}}
	@if(isset($movie))
	{{Form::text('duration', ($movie['duration']), ['class'=>'form-control'])}}
	@else
	{{Form::text('duration', '', ['class'=>'form-control'])}}
	@endif

	{{Form::label('year', 'Year:')}}
	@if(isset($movie))
	{{Form::text('year', ($movie['year']), ['class'=>'form-control'])}}
	@else
	{{Form::text('year', '', ['class'=>'form-control'])}}
	@endif

	{{Form::label('image', 'Image URL:')}}
	@if(isset($movie))
	{{Form::text('image', ($movie['image']), ['class'=>'form-control'])}}
	@else
	{{Form::text('image', '', ['class'=>'form-control'])}}
	@endif

	{{Form::label('description', 'Description:')}}
	@if(isset($movie))
	{{Form::textarea('description', ($movie['description']), ['class'=>'form-control'])}}
	@else
	{{Form::textarea('description', '', ['class'=>'form-control'])}}
	@endif

    {{Form::submit('Save', ['class' => 'btn btn-primary'])}}


{!! Form::close() !!}



    @if(isset($movie))
    {{Form::open(['route' => ['movies.destroy', $movie->id], 'method' => 'POST'])}}
    {{Form::hidden('_method', 'DELETE')}}


	{{ csrf_field() }}

	{{Form::submit('Delete', ['class' => 'btn btn-danger'])}}
	@endif
    
{!! Form::close() !!}


@endsection


{{--
 @if(isset($movie))
}
}
<form class="container" action="{{ route('movies.update', $movie->id) }}" method="POST">
<input type="hidden" name="_method" value="PUT">
@else
<form action="{{ route('movies.store') }}" method="POST">
@endif




	{{ csrf_field() }}

	<label>Title:</label>
	@if(isset($movie))
	<input  value="{{ $movie['title'] }}" class="form-control" type="text" name="title">
	@else
	<input class="form-control" type="text" name="title">
	@endif

	<label>Duration:</label>
	@if(isset($movie))
	<input  value="{{ $movie['duration'] }}" class="form-control" type="number" name="duration">
	@else
	<input  class="form-control" type="number" name="duration">
	@endif

	<label>Year:</label>
	@if(isset($movie))
	<input  value="{{ $movie['year'] }}" class="form-control" type="text" name="year">
	@else
	<input  class="form-control" type="text" name="year">
	@endif

	<label>Image URL:</label>
	@if(isset($movie))
	<input  value="{{ $movie['image'] }}" class="form-control" type="text" name="image">
	@else
	<input  class="form-control" type="text" name="image">
	@endif

	<label>Description:</label>
	@if(isset($movie))
	<textarea class="form-control" name="description" cols="30" rows="10">{{
	$movie['description'] }}</textarea>
	@else
	<textarea class="form-control" name="description" cols="30" rows="10"></textarea>
	@endif
	
	<input class="btn btn-primary" type="submit" name="Save">
</form>

@if(isset($movie))
<form action="{{ route('movies.destroy', $movie->id) }}" method="POST">
<input type="hidden" name="_method" value="DELETE">


{{ csrf_field() }}

<button type="submit" class="btn btn-danger">Delete</button>
</form>
@endif


{!! Form::open(['url' => '/formos', 'method'=>"GET"]) !!}
    {{Form::text('email','example@gmail.com', ['class'=>'awesome', 'id'=>'geras'])}}
    {{Form::submit('Enter')}}
    {{Form::text('username','username-example')}}
{!! Form::close() !!}
--}}
 