@extends("layout.main")

@section("content")

@if(count($errorMessages)>0)
	<h3>Yra klaidu</h3>
	<ul>
		@foreach($errorMessages->all() as $error)
		<li>{{$error}}</li>
		@endforeach
	</ul>
@endif


<h2>Form :)</h2>


{!! Form::open(['url' => '/formos', 'method'=>"POST"]) !!}

	{{Form::label('name', 'Name')}}
	{{Form::text('name', '', ['class'=>'form-control'])}}

	{{Form::label('email', 'E-Mail Address')}}
    {{Form::text('email','example@gmail.com', ['class'=>'form-control'])}}

    {{Form::label('email-confirm', 'confirm E-Mail Address')}}
    {{Form::text('email-confirm','example@gmail.com', ['class'=>'form-control'])}}

    {{Form::submit('Enter')}}
    
{!! Form::close() !!}

@endsection