@extends("layout.main")

@section("content")



	@if(isset($user))
	{{Form::open(['route' => ['user.update', $user->id], 'method' => 'POST'])}}
	{{Form::hidden('_method', 'PUT')}}
	@else
	{{Form::open(['route' => 'user.store', 'method' => 'POST'])}}
	@endif

	{{ csrf_field() }}

	{{Form::label('name', 'User:')}}
	@if(isset($user))
	{{Form::text('name', ($user['name']), ['class'=>'form-control'])}}
	@else
	{{Form::text('name', '', ['class'=>'form-control'])}}
	@endif

	{{Form::label('email', 'Email:')}}
	@if(isset($user))
	{{Form::text('email', ($user['email']), ['class'=>'form-control'])}}
	@else
	{{Form::text('email', '', ['class'=>'form-control'])}}
	@endif




	@if(isset($user))
	{{Form::label('is_admin', "Is Admin?")}}
	{{Form::hidden('is_admin', 0 )}}
	{{Form::checkbox('is_admin', 1 , $user->is_admin)}}
	@else

	@endif

	

    {{Form::submit('Save', ['class' => 'btn btn-primary'])}}

    


{!! Form::close() !!}


	@if(isset($user))
    {{Form::open(['route' => ['user.destroy', $user->id], 'method' => 'POST'])}}
    {{Form::hidden('_method', 'DELETE')}}


	{{ csrf_field() }}

	{{Form::submit('Delete', ['class' => 'btn btn-danger'])}}
	@endif
    
    
{!! Form::close() !!}

@endsection

