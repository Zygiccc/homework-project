@extends("layout.main")

@section("content")
<div class="container">
	<h1>Vartotojai</h1>

	
		<div class="col-md-12">
			
		</div>
	<div class="col-md-10 col-md-offset-1">

            <div class="panel panel-default panel-table">
              <div class="panel-heading">
                <div class="row">
                  <div class="col col-xs-6">
                    <h3 class="panel-title">Users</h3>
                  </div>
                </div>
              </div>
              <div class="panel-body">
                <table class="table table-striped table-bordered table-list">
                  <thead>
                    <tr>
                        <th><em class="fa fa-cog"></em></th>
                        <th class="hidden-xs">ID</th>
                        <th>Name</th>
                        <th>Email</th>
                        <th>Created at</th>
                    </tr> 
                  </thead>
                  <tbody>
                  	@foreach($user as $user)
                          <tr>
                            <td align="center">
                              <a href="{{ route('user.edit', $user->id) }}" class="btn btn-default"><em class="fa fa-pencil"></em></a>
                              
                              {{Form::open(['route' => ['user.destroy', $user->id], 'method' => 'POST'])}}
							  {{Form::hidden('_method', 'DELETE')}}

								{{ csrf_field() }}

								{{Form::submit('Delete', ['class' => 'btn btn-danger'])}}
							    
								{!! Form::close() !!}

                            </td>
                            <td class="hidden-xs">{{ $user->id }}</td>
                            <td>{{ $user->name }}</td>
                            <td>{{ $user->email }}</td>
                            <td>{{ $user->created_at }}</td>
                          </tr>
					@endforeach

                        </tbody>
                </table>
            </div>
        </div>
	</div>


@endsection