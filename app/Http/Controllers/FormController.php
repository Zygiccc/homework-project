<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use App\Http\Requests;

class FormController extends Controller {

	public function index(){

		$errorMessages = [];

		return view('form', compact('errorMessages'));
	}

	public function displayRequest(Request $request) {
		// var_dump($request->request);

		$rules = array(
				'name' => 'required',
				'email' => 'required|email',
				'email-confirm' => 'required|email|same:email'
			);

		$messages = array(
			'required' => 'Laukelis :attribute turi buti uzpildytas',
			'same' => ':others turi sutapti',
			'email' => 'cia ne emailas laukelyje: :attribute'
			);

		$validator = Validator::make($request->all(), $rules, $messages);
		$errorMessages = [];

		if($validator->passes()) {
			echo "viskas gerai";
		} else {
			$errorMessages = $validator->messages();
		}

		return view('form', compact('errorMessages'));
	}
}