<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use Auth;

class MovieController extends Controller
{
    public function __construct() {
        $this->middleware('auth', ['only' => 'create']);
        // $this->middleware('auth', ['except' => 'index', 'show']);
    }




    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $movies = \App\Movie::all();


        return view('movies.index', compact('movies'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('movies.create');
        // if(\Auth::guest()) {
        //     return redirect("/login");
        // } else {
        //     return view('movies.create');
        // }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $movie = new \App\Movie();
        $movie->title = $request->title;
        $movie->duration = $request->duration;
        $movie->year = $request->year;
        $movie->image = $request->image;
        $movie->description = $request->description;

        $movie->save();

        // Issaugojimas v2
        // $success = \App\Movie::create($request->all());

        return redirect()->route('movies.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $movie = \App\Movie::find($id);

        return view('movies.show', compact('movie'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $movie = \App\Movie::find($id);
        if(Auth::guest()) {
            return redirect("/login");
        } else {
            $movie = \App\Movie::find($id);
            return view('movies.create', compact('movie'));
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        \App\Movie::find($id)->update($request->all());

        return redirect()->route('movies.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        \App\Movie::find($id)->delete();

        return redirect()->route('movies.index');
    }
}
